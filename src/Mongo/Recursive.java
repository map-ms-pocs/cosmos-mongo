package Mongo;

import org.bson.Document;
import org.bson.conversions.Bson;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.time.LocalTime;

import com.mongodb.MongoBulkWriteException;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.InsertManyOptions;
import com.mongodb.client.result.UpdateResult;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.*;

/**
 * Simple application that shows how to use Azure Cosmos DB for MongoDB API in a Java application.
 *
 */
public class Recursive {
	
    public static void main(String[] args)
    {
	/*
	* Replace connection string from the Azure Cosmos DB Portal
        */
        MongoClientURI uri = new MongoClientURI("mongodb://XXXX");
        long startTime = 0;
        long stopTime = 0;
        String container = "tir4";
        try {
            BufferedReader br = new BufferedReader(new FileReader("companies.json"));
            List lines = Files.lines(Paths.get("companies.json")).collect(Collectors.toList());
            int count = lines.size();
            System.out.println("Lines of file: " + count);
            
            String line;
            List<Document> documents = new ArrayList<Document>();
            while ((line = br.readLine()) != null) {
                documents.add(new Document(Document.parse(line)));
            }            
            MongoClient mongoClient = new MongoClient(uri); 
            Bson filter = or(lte("number_of_employees", 100), eq("number_of_employees", null));
            Bson query = set("number_of_employees", 1000);
            startTime = System.currentTimeMillis();
            System.out.println("Start time: " + LocalTime.now());
            //Uncomment following line if you want insert multiple documents instead of updating them
            //InsertMultiple(documents, container, mongoClient); 
            //Comment folowing line if you just want insert documents
            UpdateMultiple(container, mongoClient, filter, query);  
                     	
            stopTime = System.currentTimeMillis();  
            System.out.println("Time taken " + ((stopTime - startTime) / 1000) + " seconds");
            System.out.println("End time: " + LocalTime.now());
            System.out.println( "Completed successfully" );
            br.close();
            
        } 
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        catch (InterruptedException e){
            System.out.println(e.getMessage());
        }
        
    }

    public static void InsertMultiple (List<Document> docs, String container, MongoClient mongoClient) throws InterruptedException {
        try {  
             // Get database
             MongoDatabase database = mongoClient.getDatabase("Database");
             // Get collection
             MongoCollection<Document> collection = database.getCollection(container);
             collection.insertMany(docs, new InsertManyOptions().ordered(false));
        }catch(MongoBulkWriteException e){ 
            System.out.println( "ERROR" ); 
            List<Document> errorDocs = new ArrayList<Document>();
            int errorsNumber = e.getWriteErrors().size();
            int index = 0;            
            System.out.println("Errores: " + errorsNumber);
            for (int i = 0; i < errorsNumber; i++){
                index = e.getWriteErrors().get(i).getIndex(); 
                errorDocs.add(docs.get(index));     
            }
            Thread.sleep(1250);
            InsertMultiple(errorDocs, container, mongoClient);
        }
    }

    public static void UpdateMultiple (String container, MongoClient mongoClient, Bson filter, Bson query) throws InterruptedException {
        try {  
             // Get database
             MongoDatabase database = mongoClient.getDatabase("Database");
             // Get collection
             MongoCollection<Document> collection = database.getCollection(container);
             UpdateResult r = collection.updateMany(filter, query);
             System.out.println("UpdateMany Status : " + r.wasAcknowledged());
		     System.out.println("No of Record Modified : " + r.getModifiedCount());
        }catch(MongoWriteException e){ 
            System.out.println( "Exception Update" );               
            System.out.println("Errores: " + e.getError().getMessage());
            Thread.sleep(1250);
            UpdateMultiple(container, mongoClient, filter, query);
        }
    }
}


