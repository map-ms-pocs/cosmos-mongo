package Mongo;

import org.bson.Document;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.time.LocalTime;

import com.mongodb.MongoBulkWriteException;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.InsertManyOptions;

/**
 * Simple application that shows how to use Azure Cosmos DB for MongoDB API in a Java application.
 *
 */
public class Concept {
	
    public static void main(String[] args) throws InterruptedException
    {
	/*
	* Replace connection string from the Azure Cosmos DB Portal
        */
        MongoClientURI uri = new MongoClientURI("mongodb://XXXX");
        //List<InsertOneModel<Document>> docs = new ArrayList<>();
        List<Document> docs = new ArrayList<Document>();
        MongoClient mongoClient = null;
        long startTime = 0;
        long stopTime = 0;
        String container = "ti5";
        try {
            BufferedReader br = new BufferedReader(new FileReader("companies.json"));
            List lines = Files.lines(Paths.get("companies.json")).collect(Collectors.toList());
            int count = lines.size();
            System.out.println("Lines of file: " + count);
            
            String line;

            while ((line = br.readLine()) != null) {
                docs.add(new Document(Document.parse(line)));
            }
            mongoClient = new MongoClient(uri);        
            
            // Get database
            MongoDatabase database = mongoClient.getDatabase("Database");

            // Get collection
            MongoCollection<Document> collection = database.getCollection(container);
            startTime = System.currentTimeMillis();
            System.out.println("Start time: " + LocalTime.now());
            collection.insertMany(docs, new InsertManyOptions().ordered(false));
        	
            System.out.println( "Completed successfully" );
            stopTime = System.currentTimeMillis();  
            System.out.println("Time taken " + (stopTime - startTime) + " milliseconds");
            System.out.println("End time: " + LocalTime.now());
            br.close();
            
        } 
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        catch (MongoBulkWriteException e){
            System.out.println( "ERROR" ); 
            int errorsNumber = e.getWriteErrors().size();
            int index = 0;
            int retryMs = 0;
            int retryAdjust = 20;
            String msg = "";
            System.out.println("Errores: " + errorsNumber);
            MongoDatabase database = mongoClient.getDatabase("Database");

            // Get collection
            MongoCollection<Document> collection = database.getCollection(container);
            for (int i = 0; i < errorsNumber; i++){
                msg = e.getWriteErrors().get(i).getMessage();
                String[] parts = msg.split(",");
                retryMs = Integer.parseInt(parts[1].substring(parts[1].lastIndexOf("=") +1));
                retryMs = retryMs/retryAdjust;
                if (i == 0){
                    System.out.println("Retry MS: " + Integer.parseInt(parts[1].substring(parts[1].lastIndexOf("=") +1)));
                    System.out.println("Retry adjusted: " + retryAdjust);
                }
                //Thread.sleep(3);
                index = e.getWriteErrors().get(i).getIndex(); 
                msg = e.getWriteErrors().get(i).getMessage();     
                collection.insertOne(docs.get(index));
                //Document stats = database.runCommand(new Document("getLastRequestStatistics", 1));
                //Double requestCharge = stats.getDouble("RequestCharge");
                //System.out.println("RUs used: " + requestCharge); 

            }
            stopTime = System.currentTimeMillis(); 
            System.out.println("Time taken " + ((stopTime - startTime) / 1000) + " seconds");
            System.out.println("End time: " + LocalTime.now());
            System.out.println("Indice: " + index );
        }
    }
}
