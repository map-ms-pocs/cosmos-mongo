---
author: Rafael Álvarez

email: <raalvar@microsoft.com>

languages:
- Java
products:
- Azure Cosmos DB

---

# Descripción
Azure Cosmos DB es un servicio de base de datos con varios modelos distribuido de forma global de Microsoft. Con tan solo un clic, Cosmos DB permite escalar de forma elástica e individual el rendimiento y el almacenamiento en cualquier número de regiones de Azure a nivel mundial. Puede escalar de forma elástica el rendimiento y almacenamiento, y sacar provecho del rápido acceso a datos (menos de 10 milisegundos) mediante la API que prefiera, entre las que se incluyen: SQL, MongoDB, Cassandra, Tables o Gremlin. Cosmos DB proporciona completos Acuerdos de Nivel de Servicio (SLA) con garantía de rendimiento, latencia, disponibilidad y consistencia, algo que no ofrecen otros servicios de base de datos.

Este proyecto pretende demostrar como realizar inserciones y actualizaciones masivas de datos en **Cosmos DB** cuando se utiliza la API de MongoDB. Se incluyen dos archivos. El archivo Concept.java pretende demostrar conceptualmente cuál sería la manera de tratar con el error 16500 en Cosmos DB que indica que se están realizando más peticiones que las RU asignadas a la base de datos o la colección.

## Ejecutando el ejemplo

* El código proporcionado se ha desarrollado con Visual Studio Code para ejecutarlo basta con cumplir con los siguientes pre-requisitos:

   * Cuenta Activa de Azure con una base de datos Cosmos DB. Alternatively, you can use the [Azure Cosmos DB Emulator](https://docs.microsoft.com/azure/cosmos-db/local-emulator) for this tutorial.
   * Visual Studio Code con Java Extension Pack instalado [Java Extension Pack](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack)
   * Si no deseas instalar Visual Studio Code basta con instalar JDK 1.7+ y Maven
   

* Una vez instalados los prerequisitos es necesario clonar el repositorio con el siguiente comaando: `git clone https://raalvar@bitbucket.org/map-ms-pocs/cosmos-mongo.git`

* A continuación , sustituya en Concept.java y Recursive.java la cadena de conexión dentro de la variable **MongoClientURI**. 

* Si utilizas VScode bastará con ejecutar el programa para que VScode lo compile y resuelva las dependencias automáticamente. 

* Si no utilizas Vscode, ejecuta el comando `mvn package` para compilar y resolver las dependencias.

* Una vez compilada, ejecuta el siguiente comando  `mvn exec:java -D exec.mainClass=Mongo.Concept` o `mvn exec:java -D exec.mainClass=Mongo.Recursive` para ejecutar la aplicación.

## Test Realizados 

El dataset que se ha empleado para realizar la inserción/actualización masiva es un dataset con 18001 documentos que ocupa 74,6 MB. El fichero que contiene los datos se llama companies.json.

### Inserción múltiple con inserción individual

Se trata del concepto más fácil de entender pues asume una inserción masiva utilizando el método **InsertMany**. Cuando el número de inserciones por segundo supera las RU asignadas se captura la excepción **MongoBulkWriteException**. Esta excepción muestra todas las inserciones que han fallado así como los el tiempo que es necesario esperar antes reintentar insertar los datos de nuevo. 

El índice del elemento qur no se ha podido insertar en el array se obtiene con el siguiente código: `index = e.getWriteErrors().get(i).getIndex();`. Una vez obtenido el elemento lo insertaremos de forma individual con `collection.insertOne(docs.get(index));`

Nótese que en este caso se ha comentado el sleep. Esto se debe a que utilizando el **insertOne**, el documento se envía y se espera a la respuesta hasta enviar el siguiente. Teniendo en cuenta que se ha desplegado Cosmos Db en Dublín y que la latencia que existe entre Madrid y Dublín es ~50 ms y el RTT ~100 ms no es necesario establecer esperas, ya que con el tiempo de espera que propone Cosmos es inferior a 100 ms. El tiempo de espera tendría sentido si las inserciones o actualizaciones con este procedimiento se hicieran desde una máquina virtual de Azure a CosmosDb.

#### Resultados

Tiempo utilizado: **778 segundos**

RU configuradas: 1000

![Suma total de RUs por minuto !](/static/insertone.png "Suma total de RUs por minuto")

### Inserción múltiple recursiva

Una forma de mejorar el algortimo anterior, el cual, como se ha explicado es muy sencillo de entender, es realizar una modificación en el procedimiento utilizando **Recursividad**. El procedimiento comienza al igual que en el caso anterior, haciendo uso del método **InsertMany**, cuando se produce la excepción generamos, la capturamos y generamos una nueva lista de documentos con los documentos que no han podido ser insertados, y volvemos a llamar método **InsertMultiple** con la nueva lista de documentos creada. 

En este caso, es necesario establecer un tiempo de espera, ya que éste método de Mongo envía todos los datos sin esperar respuesta, por lo que alcanzaremos el límite de RU asignados y será necesario espera un tiempo mínimo de, al menos, 1 segundo. Es necesario puntualizar, que este tiempo, deberá ser ajustado teniendo en cuenta las características de los datos enviados. Una forma de ajustarlo es chequear las RU utilizadas por el método InsertMany mediante el siguiente código: 
```Document stats = database.runCommand(new Document("getLastRequestStatistics", 1));
   Double requestCharge = stats.getDouble("RequestCharge");
```

#### Resultados

Tiempo utilizado: **385 segundos**

RU configuradas: 1000

**Mejora: 49,5 %**

![Suma total de RUs por minuto utilizando inserción recursiva !](/static/insertmany.png "Suma total de RUs por minuto utilizando inserción recursiva")

### Actualización múltiple 

La actualización en Mongo es un caso más específico que la actualización. Ya que una actualización masiva se compone de un filtro que será utilizado para buscar los elementos y la posterior query en la que actualizaremos los elementos. Es necesario tener en cuenta, que en un modelo basado en RU como en el que se basa Cosmos DB la búsqueda del filtro consume RUs. 

Si hacemos una consulta al conjunto de datos que vamos a actualizar y verificamos las RUs utilizadas veremos algo como lo que se muestra en la siguiente imagen:

![RUs utilizadas haciendo una consulta sobre los datos que cambiaremos !](/static/rus_used.png "RUs utilizadas haciendo una consulta sobre los datos que cambiaremos")

Tal y como se puede areciar, las RUs necesarias para aplicar un filtro sobre la totalidad de los datos que actualizaremos requiere 1384 RUs, que es superior al valor utilizado anteriormente. Como se verá en los resultados utilizando un valor de 1000 RUs se penalizará el rendimiento muchísimo, debido a que la búsqueda de los elementos consume más de 1000 RUs por lo que sería necesario establecer tiempos de espera superiores a 2 segundos, de lo contrario Cosmos DB producirá múltiples errores, ya que estaremos sobrepasando las RUs asignadas.

#### Resultados

Tiempo utilizado: **2410 segundos**

RU configuradas: 1000

![Actualización múltiple utilizando 1000 RUs !](/static/updatemany_1000.png "Actualización múltiple utilizando 1000 RUs")

Se observa en la gráfica como los reintentos van elevando progresivamente las RUs necesarias, precisamente al final del proceso se producen múltiples errores y Cosmos DB requiere tiempos de espera superiores a 3,5 segundos.

Teniendo en cuenta que se requería en la búsqueda más de 1000 RUs se aumentó el límite a 2000 RU obteniéndose los siguientes resultados.

Tiempo utilizado: **726 segundos**

RU configuradas: 2000

**Mejora: 70 %**

![Actualización múltiple utilizando 2000 RUs !](/static/updatemany_2000.png "Actualización múltiple utilizando 2000 RUs")

Nótese como la gráfica es coherente con las gráficas mostradas en los procesos de inserción, en el que se produce un pico máximo alrededors de 40000 peticiones por minuto y luego va descendiendo, frente a la mostrada cuando se configuró 1000 RU, en la que se puede ver que el número de RUs va incrementándonse debido a los reintentos múltiples efectuados al no disponer de RU para hacer la búsqueda y posterior actualización. Se puede apreciar claramente la penalización de rendimiento obtenida utilziando sólo 1000 RU, ya que el tiempo, empleando el doble de RUs es 3 veces menos.

### Actualización múltiple con autoescalado

Finalmente se configuró la capacidad de [Autoescalado](https://docs.microsoft.com/en-us/azure/cosmos-db/provision-throughput-autoscale). Dicha capacidad nos permiete escalar muy rápida entre un umbral máximo que llamaremos Tmax y un umbral mínimo que será 0,1 * Tmax.

El autoescalado nos va a permitir adaptarnos muy rápido a picos impredecibles de tráfico, siendo el mínimo necesario para configurar de 4000 RUs.

En este caso, como se verá los resultados son coherentes.

#### Resultados

Tiempo utilizado: **293 segundos**

RUs máximas utilizadas: 4000

**Mejora respecto a 2000 RUs sin autoescalado: 60% **

![Actualización múltiple utilizando 4000 RUs (autoescalado) !](/static/updatemany_4000.png "Actualización múltiple utilizando 4000 RUs (autoescalado)")

La siguiente imagen muestra como se aplica el autoescalado, tanto hacia arriba como hacia abajo.

![RUs aprovisionadas con autoescalado !](/static/throughput_used.png "RUs aprovisionadas con autoescalado")

Con el autoescalado se facturará por hora, las RUs utilizadas en cada momento.

## Más información

- [Azure Cosmos DB](https://docs.microsoft.com/azure/cosmos-db/introduction)
- [Azure Cosmos DB for MongoDB API](https://docs.microsoft.com/en-us/azure/cosmos-db/mongodb-introduction)
- [MongoDB Java driver](https://docs.mongodb.com/ecosystem/drivers/java/)
